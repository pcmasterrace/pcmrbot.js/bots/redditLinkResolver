module.exports = {
	apps : [{
		name      : 'bot/redditLinkResolver',
		script    : 'build/resolver.js',
		env: {
			// Either inject the values via environment variables or define them here
			TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || "",
			BOT_LINKRESOLVER_SUBREDDIT: process.env.BOT_LINKRESOLVER_SUBREDDIT || "",
			BOT_LINKRESOLVER_DB_DIALECT: process.env.BOT_LINKRESOLVER_DB_DIALECT || undefined,
			BOT_LINKRESOLVER_DB_HOST: process.env.BOT_LINKRESOLVER_DB_HOST || undefined,
			BOT_LINKRESOLVER_DB_NAME: process.env.BOT_LINKRESOLVER_DB_NAME || undefined,
			BOT_LINKRESOLVER_DB_USERNAME: process.env.BOT_LINKRESOLVER_DB_USERNAME || undefined,
			BOT_LINKRESOLVER_DB_PASSWORD: process.env.BOT_LINKRESOLVER_DB_PASSWORD || undefined,
			BOT_LINKRESOLVER_DB_PORT: process.env.BOT_LINKRESOLVER_DB_PORT || undefined,
			BOT_LINKRESOLVER_DB_PATH: process.env.BOT_LINKRESOLVER_DB_PATH || undefined,
			BOT_LINKRESOLVER_DB_DROP_TABLES_ON_START: process.env.BOT_LINKRESOLVER_DB_DROP_TABLES_ON_START || false
		}
	}]
};
