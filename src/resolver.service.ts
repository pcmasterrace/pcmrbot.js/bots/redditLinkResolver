import { Service, Context, Errors } from "moleculer";
import * as Sequelize from "sequelize";
import * as yn from "yn";
import * as path from "path";

class ResolverService extends Service {
	protected subreddit: string;
	protected db: Sequelize.Sequelize;
	protected WhitelistedSub: Sequelize.Model<{}, {}>;

	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "bot.linkResolver",
			version: 2,
			dependencies: [
				{name: "reddit-rt.posts", version: 2},
				{name: "reddit.post", version: 1},
				{name: "reddit.media", version: 1},
				{name: "reddit.user", version: 1}
			],
			events: {
				"reddit-rt.*": this.eventHandler
			},
			created: this.serviceCreated
		});
	}

	async eventHandler(originalPost) {
		this.logger.debug(originalPost.subreddit, this.subreddit, originalPost.subreddit != this.subreddit);
		if (originalPost.subreddit != this.subreddit) return;

		let searchBody: string;

		if(originalPost.name.startsWith("t1")) {
			searchBody = originalPost.body;
		} else if (originalPost.is_self) {
			searchBody = originalPost.selftext;
		} else {
			searchBody = originalPost.url;
		}

		let urls = /(https?:\/\/v\.redd\.it\/\w+\b)/gmi.exec(searchBody);
		
		this.logger.debug(urls);

		if (urls !== null) {
			// First entry is the matched string, which we don't need
			urls.shift();

			let shouldRemove = false;

			for (let url of urls) {
				let destination = await this.broker.call("v1.reddit.media.getSourceInfo", {
					url: url
				});
				this.logger.debug(destination);

				if (await this.WhitelistedSub.findOne({where: {subreddit: destination.subreddit}}) === null) {
					shouldRemove = true;
					break;
				}
			}
			this.logger.debug("should remove: ", shouldRemove);

			if (shouldRemove) {
				await this.broker.call("v1.reddit.post.removePost", {
					postId: originalPost.name
				});

				let removalText = await this.broker.call("v1.reddit.toolbox.renderRemovalMessage", {
					subreddit: originalPost.subreddit,
					reasons: ["Rule 3"],
					author: originalPost.author,
					kind: originalPost.name.startsWith("t1") === "t1" ? "comment" : "submission"
				});

				if (originalPost.name.startsWith("t1")) {
					await this.broker.call("v1.reddit.user.sendMessage", {
						username: originalPost.author,
						subject: `/r/${originalPost.subreddit}`,
						message: `${removalText}\n\n---\n\n[Link to your comment](${originalPost.permalink})`
					});
				} else {
					await this.broker.call("v1.reddit.post.reply", {
						postId: originalPost.name,
						message: removalText,
						distinguish: true
					});
				}
			}
		}

		this.logger.debug("end post");
	}

	async serviceCreated() {
		this.subreddit = process.env.BOT_LINKRESOLVER_SUBREDDIT;
		if(!this.subreddit) throw new Errors.MoleculerError("You didn't supply a subreddit for the link resolver bot, you dingus");

		this.db = new Sequelize({
			dialect: process.env.BOT_LINKRESOLVER_DB_DIALECT || undefined,
			database: process.env.BOT_LINKRESOLVER_DB_NAME || undefined,
			host: process.env.BOT_LINKRESOLVER_DB_HOST || undefined,
			username: process.env.BOT_LINKRESOLVER_DB_USERNAME || undefined,
			password: process.env.BOT_LINKRESOLVER_DB_PASSWORD || undefined,
			port: Number(process.env.BOT_LINKRESOLVER_DB_PORT) || undefined,
			storage: process.env.BOT_LINKRESOLVER_DB_PATH || undefined,
			operatorsAliases: false,
			logging: false
		});

		this.WhitelistedSub = this.db.import(path.join(__dirname, "./whitelistedSub.model"));

		await this.db.sync({force: yn(process.env.BOT_LINKRESOLVER_DB_DROP_TABLES_ON_START, {default: false})});
	}
}

module.exports = ResolverService;