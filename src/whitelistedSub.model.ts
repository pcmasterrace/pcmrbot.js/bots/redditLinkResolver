import * as Sequelize from "sequelize";

module.exports = function(sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes) {
	return sequelize.define("WhitelistedSub", {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		subreddit: DataTypes.STRING
	});
}